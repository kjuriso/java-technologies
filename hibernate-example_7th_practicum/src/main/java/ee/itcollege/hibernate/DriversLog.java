package ee.itcollege.hibernate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DriversLog {
	
	public static final String DRIVERS = "drivers";
	private static final Logger DRIVERS_LOG = LogManager.getLogger(DRIVERS);

	public static void log(String message){
		DRIVERS_LOG.info(message);
	}

}


