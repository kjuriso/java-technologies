package ee.itcollege.newsreader;

import java.io.FileWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Main_NewsReader {

	public static void main(String[] args) throws Exception {

		Document document = Jsoup.connect("http://www.eestielu.delfi.ee").get();
		Elements headlines = document.select(".article-title");

		List<String> headlinesList = new ArrayList<String>();

//		Elements dates = headlines.select("span.date");
//
//		int j = 0;
//		String[] d = new String[dates.size()];
//		for (Element date1 : dates) {
//			d[j++] = date1.attr("data-time");
//		}
		Date date = new Date();
		String finalDate = "";

		for (int i = 0; i < headlines.size(); i++) {

			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss a");
			sdf.setTimeZone(TimeZone.getTimeZone("GMT+2"));
			finalDate = sdf.format(date);

		}

		for (Element headline : headlines) {
			String h1 = headline.select("*").text().toString();
			String h2 = headline.absUrl("href");

			headlinesList.add(h1);
			headlinesList.add(h2);
			headlinesList.add(finalDate);

		}

		try (Writer writer = new FileWriter("News.txt")) {
			Gson gson = new GsonBuilder().create();
			gson.toJson(headlinesList, writer);

		}
		
		for (Object hl : headlinesList) {
			System.out.println(hl);

		}
	}
}