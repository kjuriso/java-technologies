package ee.itcollege.tetris.service;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Test;

import ee.itcollege.tetris.service.ConfigurationService;

public class ConfigurationServiceTest {

	@Test
	public void test() throws FileNotFoundException, IOException {
		
		ConfigurationService service = new ConfigurationService();
		try {
		service.loadFile("test/test.properties");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		assertEquals("value", service.getStringValue("key"));
		assertEquals(123, service.getIntValue("numeric"));
		assertEquals("ajee", service.getStringValue("test.with.dots"));
	}

}
