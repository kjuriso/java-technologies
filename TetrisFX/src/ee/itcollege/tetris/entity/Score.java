package ee.itcollege.tetris.entity;

import java.io.Serializable;

public class Score implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String playerName;
	private int score;

	public Score(String playerName, int score) {
		super();
		setPlayerName(playerName);
		setScore(score);
	}

	public String getPlayerName() {
		return playerName;
	}

	private void setPlayerName(String playerName) {
		if (null == playerName || "".equals(playerName.trim())) {
			throw new IllegalArgumentException("empty name!");
		}
		this.playerName = playerName.trim();
	}

	public int getScore() {
		return score;
	}

	private void setScore(int score) {
		if (score < 0) {
			throw new IllegalArgumentException("negative score!");
		}
		this.score = score;
	}

}
