package ee.itcollege.hibernate.entity;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Entity
 * 
 */
@Entity
@Table(name = "dog")
public class Dog {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column
	private String make;

	@ManyToOne
	private Owner owner;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Override
    public String toString() {
        String owner = "none";
        if (getOwner() != null) {
            owner = getOwner().getOwnerName();
        }
        return String.format("%d - %s - driver: %s", getId(), getMake(), owner);
    }

	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	public Owner getOwner() {
		return owner;
	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

}
