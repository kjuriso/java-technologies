
package ee.itcollege.hibernate;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import ee.itcollege.hibernate.entity.Dog;
import ee.itcollege.hibernate.entity.Owner;

public class JPAproject {

	static EntityManager em;

	public static void listDogs() {
		@SuppressWarnings("unchecked")
		List<Dog> dogs = em.createNativeQuery("select d from Dog d order by d.id",
				Dog.class).getResultList();
		for (Dog dog : dogs) {
			System.out.println(dog);
		}		System.out.println();
	}

	public static void listOwners() {
		@SuppressWarnings("unchecked")
		List<Owner> owners = em.createNativeQuery("select o from Owner o",
				Owner.class).getResultList();
		for (Owner owner : owners) {
			System.out.println(owner);
		}
		System.out.println();
	}

	public static void addDog() {
		System.out.println("What is the breed of the dog?");
		String make = TextIO.getlnString();

		Dog dog = new Dog();
		dog.setMake(make);

		System.out.println("What is the name of the owner?");
		String ownerName = TextIO.getlnString();

		Owner owner = new Owner();
		owner.setOwnerName(ownerName);
		dog.setOwner(owner);

		em.persist(owner);
		em.persist(dog);

	}

	public static void main(String[] args) {

		EntityManagerFactory entityManagerFactory = Persistence
				.createEntityManagerFactory("test1");
		em = entityManagerFactory.createEntityManager();

		while (true) {
			System.out.println("\n Please choose your action:");
			System.out.println("1 - list dogs");
			System.out.println("2 - list owners");
			System.out.println("3 - add dog and owner");
			System.out.println("4 - any other number to exit");

			int choice = TextIO.getlnInt();

			em.getTransaction().begin();

			switch (choice) {
			case 1:
				listDogs();
				break;
			case 2:
				listOwners();
				break;
			case 3:
				addDog();
				break;
			default:
				em.close();
				return;
			}

			em.getTransaction().commit();
		}

	}

}
