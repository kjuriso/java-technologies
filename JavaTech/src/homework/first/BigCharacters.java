package homework.first;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;

public class BigCharacters {

	public static void main(String[] args) {

		System.out.println("Palun sisesta mingi s�na:");
		String sona = TextIO.getln();

		BufferedImage image = new BufferedImage(100, 100, BufferedImage.TYPE_BYTE_BINARY);
		Graphics graphics = image.getGraphics();
		graphics.setColor(Color.white);
		graphics.fillRect(0, 0, 100, 100);
		graphics.setColor(Color.black);
		graphics.drawString(sona.toUpperCase(), 30, 50);

		JFrame window = new JFrame()

		{
			private static final long serialVersionUID = 1L;

			@Override
			public void paint(Graphics g) {
				super.paint(g);
				g.drawImage(image, 0, 0, null);
			}
		};

		window.setSize(100, 100);
		window.setVisible(true);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		int[][] maatriks = new int[image.getWidth()][image.getHeight()];

		for (int i = 0; i < image.getWidth(); i++) {
			for (int j = 0; j < image.getHeight(); j++) {

				int rgb = image.getRGB(j, i);

				maatriks[i][j] = rgb;
			}
		}

		for (int i = 0; i < maatriks.length; i++) {
			for (int j = 0; j < maatriks.length; j++) {

				if (maatriks[i][j] == -16777216) {
					System.out.print("#");
				} else {
					System.out.print(" ");
				}

			}
			System.out.println();

		}

	}
}
