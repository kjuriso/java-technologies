package homework.third;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageToAscii {

	public static void main(String[] args) {

		new ImageToAscii();
	}

	public ImageToAscii() {

		try {
			BufferedImage img = ImageIO.read(this.getClass().getResource("heart.png"));
			GoThroughPixels(img);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

	}

	private void GoThroughPixels(BufferedImage img) {
		int w = img.getWidth();
		int h = img.getHeight();

		for (int i = 0; i < h; i++) {
			for (int j = 0; j < w; j++) {
				int rgb = img.getRGB(j, i);

				if (rgb < -1) {
					System.out.print("#");
				} else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}
	}
}