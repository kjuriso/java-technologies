package exercises2;

import static org.junit.Assert.*;
//import static org.hamcrest.core.Is.*;
import org.junit.Test;

public class DigitsTest {

	@Test(timeout = 1000)
	public void testProductSimple() {
		
		assertEquals(6, Digits.product(123));
		assertEquals(8, Digits.product(222));
		assertEquals(28, Digits.product(74));
		
		assertEquals(1, Digits.product(11111111));
		
		assertNotEquals(6, Digits.product(1234));
		//assertThat(Digits.sum(123), is(6));
		//fail("Not yet implemented");
	}
	
	@Test(timeout = 1000)
	public void testProductZero() {
		assertEquals(0, Digits.product(7504327));
		assertEquals(0, Digits.product(0));
		assertNotEquals(0, Digits.product(0725));
		assertEquals(28, Digits.product(74));
			
	}
/*	@Test(timeout = 1000)
	public void testProductMaximum() {
		int bigNumber= 1999999999;
		assertEquals((int)Math.pow(9,  String.valueOf(bigNumber).length()), Digits.product(bigNumber));
	}
*/
	@Test(timeout = 1000)
	public void testProductNegative() {
		assertEquals(10, Digits.product(-25));
		assertEquals(6, Digits.product(-123));
		assertEquals(72, Digits.product(-89));
	}

}
