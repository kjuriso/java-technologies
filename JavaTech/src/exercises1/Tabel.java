package exercises1;

public class Tabel {

	public static void main(String[] args) {

		int size = 15;
		int base = 10;

		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				
				int value =  Math.min(size - i - 1, size - j - 1);
				System.out.format("%2d", value % base);

			}
			System.out.println();
		}
	}
}

/*
 * public static void main(String[] args) { printArray(tabel(10)); }
 * 
 * public static int[][] tabel(int n) { int[][] temptabel = new int[n][n]; //
 * int el = 0; for (int i = 0; i < temptabel.length; i++) { for (int j = 0; j <
 * temptabel.length; j++) { temptabel[i][j] = 0; // el = i * j; //
 * System.out.print(el);
 * 
 * }
 * 
 * } return temptabel; }
 * 
 * public static void printArray(int matrix[][]) { for (int row = 0; row <
 * matrix.length; row++) { for (int column = 0; column < matrix[row].length;
 * column++) { System.out.print(matrix[row][column] + " "); }
 * System.out.println();
 * 
 * }
 * 
 * }
 */
